const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const taskRoutes = require("./routes/task");

const port = process.env.PORT || 5432;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use("/uploads", express.static("uploads"));

mongoose.connect("mongodb+srv://applicant:OxzdeuEXBM85=+xQnCV7U@whattodo.t1da8.mongodb.net/FSD_2022_<BRIOSO>?retryWrites=true&w=majority" , {
	useNewUrlParser: true,
	useUnifiedTopology: true,
})

let db = mongoose.connection;
	
	db.on('error', () => console.error.bind(console, "Connection Error"));
	db.once('open', () => console.log('Connected to the cloud database.'))

app.use("/users", userRoutes);
app.use("/tasks", taskRoutes);

app.listen(port, () => console.log (`Server running @ port: ${port}.`))