const express = require("express");
const router = express.Router();
const userController = require('../controllers/user')

// Create User upon first app-open
router.post("/create", (req, res) => {
	userController.createUser(req.body).then(resultFromController => res.send (resultFromController))
})

//Delete user data
router.delete("/deleteUser", (req,res) => {
	userController.deleteUserData(req.body).then(resultFromController => res.send(resultFromController))
})

//Change color theme: Current day
router.post("/changeCurrentDay", (req, res) => {
	userController.changeCurrentDay(req.body).then(resultFromController => res.send (resultFromController))
})

//Change color theme: Free Day
router.post("/changeFreeDay", (req, res) => {
	userController.changeFreeDay(req.body).then(resultFromController => res.send (resultFromController))
})

module.exports = router;