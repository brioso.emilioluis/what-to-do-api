const express = require("express");
const router = express.Router();
const taskController = require("../controllers/task");

// Create a new task
router.post("/create", (req, res) => {
  taskController
    .createTask(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Edit task
router.post("/edit", (req, res) => {
  taskController
    .editTask(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Delete task
router.delete("/delete", (req, res) => {
  taskController
    .deleteTask(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Mark task as done
router.post("/done", (req, res) => {
  taskController
    .markTaskDone(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Get tasks for today
router.post("/today", (req, res) => {
  taskController
    .getTaskToday(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Get all tasks of user
router.post("/all", (req, res) => {
  taskController
    .getAllTasks(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
