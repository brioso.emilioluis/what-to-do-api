const User = require('../models/User');
const UserAction = require('../models/UserAction');

// Create User upon first app-open
module.exports.createUser = (reqBody) => {
	let newUser = new User({
		deviceOwner: reqBody.deviceOwner,
		deviceType: reqBody.deviceType,
		deviceModel: reqBody.deviceModel,
        deviceOS: reqBody.deviceOS
	})
	
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return user;
		}
	})
}

//Delete user data
module.exports.deleteUserData = (reqBody) => {
	return User.findByIdAndDelete(reqBody.userId).then((del, error) => {
		if (error) {
			return false
		} else {
			let newUserAction = new UserAction({
				actionName: "delete_user_id",
				actionDoer: reqBody.userId,
			})
			newUserAction.save()
			return true
		}
	})
}

//Change color theme: Current day
module.exports.changeCurrentDay = (reqBody) => {
	let newUserAction = new UserAction({
		actionName: "change_current_day_color",
		actionDoer: reqBody.userId,
		oldColor: reqBody.oldColor,
		newColor: reqBody.newColor
	})
	
	return newUserAction.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

//Change color theme: Free Day
module.exports.changeFreeDay = (reqBody) => {
	let newUserAction = new UserAction({
		actionName: "change_free_day_color",
		actionDoer: reqBody.userId,
		oldColor: reqBody.oldColor,
		newColor: reqBody.newColor
	})
	
	return newUserAction.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}