const Task = require("../models/Task");
const UserAction = require("../models/UserAction");

// Create a new task
module.exports.createTask = (reqBody) => {
  let newTask = new Task({
    name: reqBody.name,
    description: reqBody.description,
    date: reqBody.date,
    taskOwner: reqBody.taskOwner,
  });

  return newTask.save().then((task, error) => {
    if (error) {
      return false;
    } else {
      let newUserAction = new UserAction({
        actionName: "add_task",
        actionDoer: reqBody.taskOwner,
        taskId: task.id,
      });
      newUserAction.save();
      return true;
    }
  });
};

//Edit task
module.exports.editTask = (reqBody) => {
  let updatedTask = {
    name: reqBody.name,
    description: reqBody.newTaskDescription,
    date: reqBody.date,
    taskOwner: reqBody.taskOwner,
    oldTaskDescription: reqBody.oldTaskDescription,
    newTaskDescription: reqBody.newTaskDescription,
  };

  return Task.findByIdAndUpdate(reqBody.taskId, updatedTask).then(
    (task, error) => {
      if (error) {
        return false;
      } else {
        let newUserAction = new UserAction({
          actionName: "edit_task",
          actionDoer: reqBody.taskOwner,
          taskId: reqBody.taskId,
          oldTaskDescription: reqBody.oldTaskDescription,
          newTaskDescription: reqBody.newTaskDescription,
        });
        newUserAction.save();
        return true;
      }
    }
  );
};

//Delete task
module.exports.deleteTask = (reqBody) => {
  return Task.findByIdAndDelete(reqBody.taskId).then((del, error) => {
    if (error) {
      return false;
    } else {
      let newUserAction = new UserAction({
        actionName: "delete_task",
        actionDoer: reqBody.taskOwner,
        taskId: reqBody.taskId,
      });
      newUserAction.save();
      return true;
    }
  });
};

//Mark task as done
module.exports.markTaskDone = (reqBody) => {
  let doneTask = {
    isDone: true,
  };

  return Task.findByIdAndUpdate(reqBody.taskId, doneTask).then(
    (task, error) => {
      if (error) {
        return false;
      } else {
        let newUserAction = new UserAction({
          actionName: "mark_task_as_done",
          actionDoer: reqBody.taskOwner,
          taskId: reqBody.taskId,
        });
        newUserAction.save();
        return true;
      }
    }
  );
};

//Get tasks for today
module.exports.getTaskToday = (reqBody) => {
  return (tasksToday = Task.aggregate([
    { $match: { taskOwner: reqBody.taskOwner } },
    { $match: { date: reqBody.date } },
  ]));
};

//Get all tasks of user
module.exports.getAllTasks = (reqBody) => {
  return (tasksToday = Task.aggregate([
    { $match: { taskOwner: reqBody.taskOwner } },
    { $group: { _id: "$date", count: { $sum: 1 } } },
  ]));
};
