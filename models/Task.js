const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
    name:{
        type: String,
        require: [true, 'Name is required.']
    },

    description: {
        type: String
    },

    date: {
        type: String,
        required: [true, 'Date is required.']
    },

    isDone: {
        type: Boolean,
        default: false
    },

    taskOwner: {
        type: String,
        required: [true, 'Task Owner is required.']
    }
})

module.exports = mongoose.model('Task', taskSchema)