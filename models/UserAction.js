const mongoose = require('mongoose')

const userActionSchema = new mongoose.Schema({
    actionName: {
        type: String,
        required: [true, 'Action name is required.']
    },
    
    actionDoer:{
        type: String,
        required: [true, 'Action doer is required.']
    },

    actionDoneDate: {
        type: Date,
        default: new Date()
    },

    taskId:{
        type: String,
    },

    taskDescription: {
        type: String
    },

    oldTaskDescription: {
        type: String
    },

    newTaskDescription : {
        type: String
    },

    oldColor : {
        type: String
    },

    newColor : {
        type: String
    }
})

module.exports = mongoose.model('UserAction', userActionSchema)