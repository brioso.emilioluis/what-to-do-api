const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    deviceOwner: {
        type: String,
        require: [true, 'Device owner is required!']
    },
    deviceType:{
        type: String,
        require: [true, 'Device Type is required.']
    },

    deviceModel: {
        type: String,
        required: [true, 'Device Model is required.']
    },

    deviceOS: {
        type: String,
        required: [true, 'Device OS is required.']
    },
    
    createdOn:{
		type: Date,
		default: new Date()
	},

    tasks: {
        type: Array
    }
})

module.exports = mongoose.model('User', userSchema)